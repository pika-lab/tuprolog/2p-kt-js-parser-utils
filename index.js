exports.Associativity = require('./src/Associativity').Associativity;
exports.DynamicLexer = require('./src/DynamicLexer').DynamicLexer;
exports.DynamicParser = require('./src/DynamicParser').DynamicParser;
exports.StringType = require('./src/StringType').StringType;
exports.PrologLexer = require('./src-gen/PrologLexer').default;
exports.PrologParser = require('./src-gen/PrologParser').default;
exports.PrologParserVisitor = require('./src-gen/PrologParserVisitor').default;
exports.PrologParserListener = require('./src-gen/PrologParserListener').default;
